

## Specification

An app that displays a list of items.
Requirements:
    - All items are stored on the server.
    - Each item can be marked as favorite. 'favorite' flag also are stored on the server
    - To provide the best UX user mustn't see any spinners or loaders after interaction with 'favorite' button. So he must see an immediate response in UI.
    - Offline support

## Architect

### Part 1

To start working according to TDD we must determine the main parts of our app before we start to write any code.
The app will consist of two main parts: ItemsService and ItemsListModule. ItemsService will be responsible for any domain logic of item (storing, fetching and sending to the server). The ItemsListModule will display items to user.
Possibly a few more candidates for tests will rise during our development.

### Part 2

All articles about TDD tell us that we must write tests before any code is written in the project. But I think that assumed interfaces of classes can be declared before you write the test. So to start working according to the TDD you must invent and declare the interface firstly.

Let's start from the domain logic part since it's the core thing and our UI module wouldn't work without it.

#### Step 1. Coding interfaces

ItemsService responsability.
    - fetch from server
    - change `isFavorite`

Declare class and functions. Do not write any implementation, just add `fataError()` for every function.

#### Step 2. Test

Create Test for the ItemsService.



### Profit of TDD

1. Your development process has the right order: Write Specification (Test) -> Write Code

2. Good architecture. You work with your code and it help you to write it better.

3. You know that your code works good even without having any implemented API.

4. During you TDD, you concentrate on one task. You are not writing code at once that handle all possible cases. You resolve one case per one iteration.
