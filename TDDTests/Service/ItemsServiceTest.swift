//
//  ItemsServiceTest.swift
//  TDDTests
//
//  Created by Slava Semeniuk on 24.10.2019.
//  Copyright © 2019 Slava. All rights reserved.
//

import XCTest
import Combine
import OHHTTPStubs
@testable import TDD

class ItemsServiceTest: XCTestCase {

    var itemsService: ItemsService!
    var urlSession: URLSession!

    override func setUp() {
        urlSession = URLSession.shared
        itemsService = ItemsService(urlSession: urlSession)
    }

    override func tearDown() {
        urlSession = nil
        itemsService = nil
    }
}

final class ItemsServiceSyncLogicTest: ItemsServiceTest {

    private var requests: [Any]!
    private var successResponseJSON = ["items": Array(repeating: ["id": 123,
                                                                  "title": "Title",
                                                                  "text": "text",
                                                                  "isFavorite": true], count: 3)]


    override func setUp() {
        super.setUp()
        requests = []
    }

    override func tearDown() {
        requests = nil
        super.tearDown()
    }

    func testRequestFromServer() {
        let expectation = XCTestExpectation(description: "Request to server")

        stub(condition: { _ in return true },
             response: { request in
                XCTAssertEqual(request.httpMethod, "GET")
                XCTAssertTrue(request.url?.path.contains("api/items") ?? false)
                expectation.fulfill()
                return OHHTTPStubsResponse(data: .init(), statusCode: 200, headers: nil)
        })

        requests += itemsService
            .syncWithServer()
            .sink(receiveCompletion: { _ in },
                  receiveValue: { _ in })

        wait(for: [expectation], timeout: 0.5)
    }

    func test200ResponseHandlingForRequest() {
        let expectation = XCTestExpectation(description: "Request to server")

        stub(condition: isPath("/api/items"),
             response: { [successResponseJSON] _ in
                expectation.fulfill()
                return OHHTTPStubsResponse(jsonObject: successResponseJSON,
                                           statusCode: 200,
                                           headers: nil)
        })

        requests += itemsService
            .syncWithServer()
            .sink(receiveCompletion: { err in
                if case .failure = err {
                    print(err)
                    XCTFail("Items service must decode response to `[Items]` on success")
                }
                expectation.fulfill()
            }, receiveValue: {
                XCTAssertEqual($0.count, 3)
                expectation.fulfill()
            })

        wait(for: [expectation], timeout: 0.5)
    }
}

final class ItemsServiceFavoriteRequestTest: ItemsServiceTest {

    private var requests: [Any]!
    private var itemToModify: Item!
    private var successResponseJSON: [String: Any] = ["id": 1,
                                                      "title": "Title",
                                                      "text": "text",
                                                      "isFavorite": true]


    override func setUp() {
        super.setUp()
        itemToModify = .init(id: 1, title: "test", text: "text", isFavorite: false)
        requests = []
    }

    override func tearDown() {
        requests = nil
        super.tearDown()
    }

    func testRequestFromServer() {
        let expectation = XCTestExpectation(description: "Request to server")

        stub(condition: { _ in return true },
             response: { [itemToModify] request in
                XCTAssertEqual(request.httpMethod, "PUT")
                XCTAssertTrue(request.url?.path.contains("api/items/\(itemToModify!.id)") ?? false)
                expectation.fulfill()
                return OHHTTPStubsResponse(data: .init(), statusCode: 200, headers: nil)
        })

        requests += itemsService
            .set(favorite: true, for: itemToModify)
            .sink(receiveCompletion: { _ in },
                  receiveValue: { _ in })

        wait(for: [expectation], timeout: 0.5)
    }

    func test200ResponseHandlingForRequest() {
        let expectation = XCTestExpectation(description: "Request to server")

        stub(condition: isPath("/api/items/\(itemToModify.id)"),
             response: { [successResponseJSON] _ in
                expectation.fulfill()
                return OHHTTPStubsResponse(jsonObject: successResponseJSON,
                                           statusCode: 200,
                                           headers: nil)
        })

        requests += itemsService
            .set(favorite: true, for: itemToModify)
            .sink(receiveCompletion: { err in
                if case .failure = err {
                    print(err)
                    XCTFail("Items service must decode response to `Items` on success")
                }
                expectation.fulfill()
            }, receiveValue: { [successResponseJSON] in
                XCTAssertEqual($0.id, successResponseJSON["id"] as! Int)
                expectation.fulfill()
            })

        wait(for: [expectation], timeout: 0.5)
    }
}
