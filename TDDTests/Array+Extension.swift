//
//  Array+Extension.swift
//  TDDTests
//
//  Created by Slava Semeniuk on 29.10.2019.
//  Copyright © 2019 Slava. All rights reserved.
//

import Foundation

extension Array {
    static func += (left: inout [Element], right: Element) {
        left.append(right)
    }
}
