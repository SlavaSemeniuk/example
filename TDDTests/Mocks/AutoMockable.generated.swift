// Generated using Sourcery 0.17.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

// swiftlint:disable line_length
// swiftlint:disable variable_name

import Foundation
@testable import TDD
import Combine
#if os(iOS) || os(tvOS) || os(watchOS)
import UIKit
#elseif os(OSX)
import AppKit
#endif














class ItemsServiceInterfaceMock: ItemsServiceInterface {

    //MARK: - syncWithServer

    var syncWithServerCallsCount = 0
    var syncWithServerCalled: Bool {
        return syncWithServerCallsCount > 0
    }
    var syncWithServerReturnValue: AnyPublisher<[Item], Error>!
    var syncWithServerClosure: (() -> AnyPublisher<[Item], Error>)?

    func syncWithServer() -> AnyPublisher<[Item], Error> {
        syncWithServerCallsCount += 1
        return syncWithServerClosure.map({ $0() }) ?? syncWithServerReturnValue
    }

    //MARK: - set

    var setFavoriteForCallsCount = 0
    var setFavoriteForCalled: Bool {
        return setFavoriteForCallsCount > 0
    }
    var setFavoriteForReceivedArguments: (favorite: Bool, item: Item)?
    var setFavoriteForReceivedInvocations: [(favorite: Bool, item: Item)] = []
    var setFavoriteForReturnValue: AnyPublisher<Item, Error>!
    var setFavoriteForClosure: ((Bool, Item) -> AnyPublisher<Item, Error>)?

    func set(favorite: Bool, for item: Item) -> AnyPublisher<Item, Error> {
        setFavoriteForCallsCount += 1
        setFavoriteForReceivedArguments = (favorite: favorite, item: item)
        setFavoriteForReceivedInvocations.append((favorite: favorite, item: item))
        return setFavoriteForClosure.map({ $0(favorite, item) }) ?? setFavoriteForReturnValue
    }

}
