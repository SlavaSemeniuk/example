//
//  ItemsLogicControllerTest.swift
//  TDDTests
//
//  Created by Slava Semeniuk on 03.11.2019.
//  Copyright © 2019 Slava. All rights reserved.
//

import XCTest
import Combine
@testable import TDD

class ItemsLogicControllerTest: XCTestCase {

    var logicController: ItemsLogicController!
    var itemsService: ItemsServiceInterfaceMock!
    var subscriptions: [AnyCancellable]!
    var currentState: ItemsView.Props!

    override func setUp() {
        super.setUp()
        itemsService = ItemsServiceInterfaceMock()
        logicController = ItemsLogicController(itemsService: itemsService)
        subscriptions = []

        let expectation = XCTestExpectation(description: "StateChange")
        subscriptions += logicController.statePubliser.sink { [weak self] in
            self?.currentState = $0
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.01)
    }

    override func tearDown() {
        itemsService = nil
        subscriptions.forEach { $0.cancel() }
        logicController = nil
        subscriptions = nil
        currentState = nil
        super.tearDown()
    }
}

final class ItemsLogicControllerItialStateTest: ItemsLogicControllerTest {
    func testInitialState() {
          guard case .loading = currentState else {
              XCTFail("Initial state must be loading")
              return
          }
      }
}

final class ItemsLogicControllerItemsLoadedTest: ItemsLogicControllerTest {

    var itemsResponse: [Item]!

    override func setUp() {
        super.setUp()
        let requestExpectation = XCTestExpectation(description: "Request")
        itemsResponse = Self.fakeItems()
        itemsService.syncWithServerClosure = { [itemsResponse] in
            requestExpectation.fulfill()
            return CurrentValueSubject<[Item], Error>
                .init(itemsResponse!)
                .eraseToAnyPublisher()
        }
        logicController.start()
        wait(for: [requestExpectation], timeout: 0.01)
    }

    override func tearDown() {
        itemsResponse = nil
        super.tearDown()
    }

    func testDataRetrive() {
        XCTAssertTrue(itemsService.syncWithServerCalled, "Items must be retreived from ItemsService")
    }

    func testProps() {
        guard case let .loaded(items) = currentState else {
            XCTFail("State must be changed to load right after get response")
            return
        }
        guard items.count == itemsResponse.count else {
            XCTFail("Count of props must be same with items that are returned from the service")
            return
        }
        zip(items, itemsResponse).forEach {
            XCTAssertEqual($0.title, $1.title)
            XCTAssertEqual($0.isButtonSelected, $1.isFavorite)
        }
    }

    func testFavoriteToggle() {
        let items = currentState.extractItems()
        let itemBeforeToggling = items!.first!
        let fakeItem = Item(id: 1239, title: "Received", text: "", isFavorite: false)
        itemsService.setFavoriteForReturnValue = CurrentValueSubject<Item, Error>
            .init(fakeItem)
            .delay(for: .seconds(1), scheduler: RunLoop.main)
            .eraseToAnyPublisher()
        itemBeforeToggling.toggleAction()
        let newItem = currentState.extractItems()!.first
        XCTAssertNotEqual(itemBeforeToggling.isButtonSelected,
                          newItem!.isButtonSelected,
                          "State must be changed right after intraction")
    }


    func testFavoriteRequestSent() {
        let fakeItem = Item(id: 1239, title: "Received", text: "", isFavorite: false)
        itemsService.setFavoriteForReturnValue = CurrentValueSubject<Item, Error>
            .init(fakeItem)
            .delay(for: .seconds(1), scheduler: RunLoop.main)
            .eraseToAnyPublisher()
        currentState.extractItems()?.first?.toggleAction()
        XCTAssertTrue(itemsService.setFavoriteForCalled, "Changin item must be done via ItemsService")
    }

    func testSuccessFavoriteRequest() {
        let fakeItem = Item(id: 1239, title: "Received", text: "", isFavorite: false)
        let requestExpectation = XCTestExpectation(description: "Request")
        itemsService.setFavoriteForClosure = { _, _ in
            requestExpectation.fulfill()
            return CurrentValueSubject<Item, Error>
                .init(fakeItem)
                .eraseToAnyPublisher()

        }
        let itemToToggle = currentState.extractItems()!.first
        itemToToggle?.toggleAction()
        wait(for: [requestExpectation], timeout: 0.01)

        let updatedItem = currentState.extractItems()?.first!
        XCTAssertNotEqual(updatedItem!.title, itemToToggle!.title)
    }

    private static func fakeItems() -> [Item] {
        return [.init(id: 0, title: "Item 1", text: "price 100$", isFavorite: false),
                .init(id: 1, title: "Item 2", text: "price 500$", isFavorite: true),]
    }

}

final class ItemsLogicControllerNoItemsTest: ItemsLogicControllerTest {

    override func setUp() {
        super.setUp()
        let requestExpectation = XCTestExpectation(description: "Request")
        itemsService.syncWithServerClosure = {
            requestExpectation.fulfill()
            return CurrentValueSubject<[Item], Error>
                .init([])
                .eraseToAnyPublisher()
        }
        logicController.start()
        wait(for: [requestExpectation], timeout: 0.01)
    }

    func testProps() {
        guard case .empty = currentState else {
            XCTFail("If service returns no items, `empty` state must be set")
            return
        }
    }
}

final class ItemsLogicControllerItemsNotLoadedTest: ItemsLogicControllerTest {

    override func setUp() {
        super.setUp()
        let requestExpectation = XCTestExpectation(description: "Request")
        itemsService.syncWithServerClosure = {
            requestExpectation.fulfill()
            let subj = CurrentValueSubject<[Item], Error>([])
            subj.send(completion: .failure(NSError(domain: "", code: 400, userInfo: nil)))
            return subj.eraseToAnyPublisher()
        }
        logicController.start()
        wait(for: [requestExpectation], timeout: 0.01)
    }

    func testProps() {
        guard case .error = currentState else {
            XCTFail("Empty state must be set if inital load are failed")
            return
        }
    }
}

fileprivate extension ItemsView.Props {
    func extractItems() -> [ItemsView.ItemProps]? {
        guard case let .loaded(items) = self else {
            return nil
        }
        return items
    }
}
