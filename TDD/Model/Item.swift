//
//  Item.swift
//  TDD
//
//  Created by Slava Semeniuk on 24.10.2019.
//  Copyright © 2019 Slava. All rights reserved.
//

import Foundation

struct Item: Identifiable, Decodable {
    let id: Int
    let title: String
    let text: String
    let isFavorite: Bool
}
