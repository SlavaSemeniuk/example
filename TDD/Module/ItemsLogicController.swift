//
//  ItemsLogicController.swift
//  TDD
//
//  Created by Slava Semeniuk on 03.11.2019.
//  Copyright © 2019 Slava. All rights reserved.
//

import Combine

final class ItemsLogicController {

    // MARK: - Typealiases
    typealias Props = ItemsView.Props
    typealias ItemProps = ItemsView.ItemProps

    // MARK: - Dependencies
    private let itemsService: ItemsServiceInterface
    
    // MARK: - Publishers
    let statePubliser = CurrentValueSubject<Props, Never>(.loading)

    // MARK: - State
    private var items: [Item] = []

    init(itemsService: ItemsServiceInterface) {
        self.itemsService = itemsService
    }

    func start() {
        itemsService
            .syncWithServer()
            .sink(receiveCompletion: { [weak self] in
                guard case let .failure(error) = $0 else { return }
                self?.generateProps(error: error)
            }, receiveValue: { [weak self] in
                self?.items = $0
                self?.generateProps()
            })
    }

    private func generateProps(error: Error? = nil) {
        guard error == nil else {
            statePubliser.send(.error)
            return
        }
        guard !items.isEmpty else {
            statePubliser.send(.empty)
            return
        }
        let props = items.enumerated().map { item in
            ItemProps(title: item.element.title,
                      summary: item.element.text,
                      isButtonSelected: item.element.isFavorite,
                      toggleAction: { [weak self] in
                        self?.toggleFavorite(forItemAt: item.offset)
            })
        }
        statePubliser.send(.loaded(items: props))
    }

    private func toggleFavorite(forItemAt index: Int) {
        let item = items[index]
        items[index] = Item(id: item.id,
                            title: item.title,
                            text: item.text,
                            isFavorite: !item.isFavorite)
        generateProps()
        itemsService
            .set(favorite: !item.isFavorite, for: item)
            .sink(receiveCompletion: { [weak self] in
                guard case let .failure(error) = $0 else { return }
//                self?.generateProps(error: error)
            }, receiveValue: { [weak self] in
                self?.items[index] = $0
                self?.generateProps()
            })
    }
}
