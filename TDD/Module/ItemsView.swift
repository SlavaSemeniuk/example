//
//  ItemsView.swift
//  TDD
//
//  Created by Slava Semeniuk on 03.11.2019.
//  Copyright © 2019 Slava. All rights reserved.
//

import SwiftUI

struct ItemsView: View {
    var body: some View {
        Text("Hello World")
            .font(.headline)
            .foregroundColor(Color.gray)
            .multilineTextAlignment(.center)
    }
}

extension ItemsView {
    enum Props {
        case loading
        case loaded(items: [ItemProps])
        case empty
        case error
    }

    struct ItemProps {
        let title: String
        let summary: String
        let isButtonSelected: Bool

        let toggleAction: () -> Void
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ItemsView()
    }
}
