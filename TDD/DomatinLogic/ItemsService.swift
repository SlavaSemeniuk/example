//
//  ItemsService.swift
//  TDD
//
//  Created by Slava Semeniuk on 24.10.2019.
//  Copyright © 2019 Slava. All rights reserved.
//

import Foundation
import Combine

/// sourcery: AutoMockable
protocol ItemsServiceInterface {
    func syncWithServer() -> AnyPublisher<[Item], Error>
    func set(favorite: Bool, for item: Item) -> AnyPublisher<Item, Error>
}

final class ItemsService: ItemsServiceInterface {

    private let urlSession: URLSession
    private let baseURL = URL(string: "https://www.mocky.io")!

    init(urlSession: URLSession) {
        self.urlSession = urlSession
    }

    func syncWithServer() -> AnyPublisher<[Item], Error> {
        urlSession
            .dataTaskPublisher(for: baseURL.appendingPathComponent("api/items"))
            .map { $0.data }
            .decode(type: ItemsResponse.self, decoder: JSONDecoder())
            .map(\.items)
            .eraseToAnyPublisher()

    }

    func set(favorite: Bool, for item: Item) -> AnyPublisher<Item, Error> {
        var request = URLRequest(url: baseURL.appendingPathComponent("api/items/\(item.id)"))
        request.httpMethod = "PUT"
        
        return urlSession
            .dataTaskPublisher(for: request)
            .map { $0.data }
            .decode(type: Item.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}

private struct ItemsResponse: Decodable {
    let items: [Item]
}
/*
 {
   "items": [
     {
       "title": "",
       "text": "",
       "isFavorite": false
     }
   ]
 }
 */

